<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Halaman Login</title>
</head>
<body class="login">
	<div class="back-up-form"></div>
	<h2>Login</h2>
	<form action="cek-login.php" method="post">
		<ul>
			<label for="username">username :</label>
			<input type="text" id="username" name="username">
		</ul>
		<ul>
			<label for="password">password :</label>
			<input type="password" id="password" name="password">
		</ul>
		<ul>
			<button type="submit" name="login">Login</button>
		</ul>
	</form>

</body>
</html>